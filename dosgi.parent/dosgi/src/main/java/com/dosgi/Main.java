package com.dosgi;

import com.dosgi.kit.StrKit;

public class Main {

	public static void main(String[] args) {
		// 设置模块的打包及加载路径
		if (StrKit.isBlank(System.getProperty("module.home")))
			System.setProperty("module.home", "../modules");

		// 启动线程1秒后通知dosgi停止
//		new Thread() {
//			public void run() {
//				try {
//					Thread.sleep(1000L);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//				Dosgi.close();
//			};
//		}.start();
		
		//启动dosgi
		DosgiLauncher.main(null);
	}

}
