package com.dosgi;

/**
 * @author dingnate
 *
 */
public enum LifeCycleState {
	INITING, INITED, STARTING, STARTED, CLOSING, CLOSED
}
