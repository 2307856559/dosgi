package com.dosgi.module;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.dosgi.IDosgiContext;

public interface IModuleContext {
	/**
	 * 启动类
	 */
	static final String BOOT_CLASS = "Boot-Class";

	/**
	 * 设置模块的类加载器，只能设置一次
	 * 
	 * @param classLoader
	 */
	public void setClassLoader(ClassLoader classLoader);

	/**
	 * 获取模块的类加载器
	 * 
	 * @return
	 */
	public ClassLoader getClassLoader();

	/**
	 * 初始化模块
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception;

	/**
	 * 启动模块
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception;

	/**
	 * 停止模块
	 * 
	 * @throws Exception
	 */
	public void stop() throws Exception;

	/**
	 * 获取模块路径
	 * 
	 * @return
	 */
	public String getFilePath();

	/**
	 * 获取Manifest.MF文件信息
	 * 
	 * @return
	 */
	public Map<String, String> getManifestAttributes();

	/**
	 * 获取模块标识名称
	 * 
	 * @return
	 */
	public String getSymbolicName();

	/**
	 * 获取模块版本
	 * 
	 * @return
	 */
	public String getVersion();

	/**
	 * 获取dosgi框架容器上下文
	 * 
	 * @return
	 */
	public IDosgiContext getContainerCtx();

	/**
	 * 通过类注册BeanDefinition，class为Key
	 * 
	 * @param clazz
	 * @param bean
	 */
	public void registry(Class<?> clazz, Object bean);

	/**
	 * 通过类获取已注册Bean
	 * 
	 * @param clazz
	 * @return
	 */
	public <T> T get(Class<T> clazz);

	/**
	 * 获取module的ExportPackages
	 * 
	 * @return
	 */
	public Set<String> getExportPackages();

	/**
	 * 获取module的ImportPackages
	 * 
	 * @return
	 */
	public Map<String, String> getImportPackages();
	
	/**
	 * 获取module的RequireModules
	 * 
	 * @return
	 */
	public Map<String, String> getRequireModules();

	/**
	 * @return the 获取RequireModules声明的模块
	 */
	public Collection<IModuleContext> getResolvedRequireModules();
	
	/**
	 * @return the 获取RequireModules声明和ImportPackages声明的模块
	 */
	public Collection<IModuleContext> getResolvedRequireModulesWithImport();
}
