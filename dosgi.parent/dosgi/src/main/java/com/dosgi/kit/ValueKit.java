package com.dosgi.kit;

import java.math.BigDecimal;

/**
 * 值处理工具类
 * 
 * @author dingnate
 *
 */
public class ValueKit {
	public final static String EMPTY = "";
	public final static Byte b = new Byte((byte) 0);
	public final static Short s = new Short((short) 0);
	public final static Integer i = new Integer(0);
	public final static Long l = new Long(0l);
	public final static Float f = new Float(0f);
	public final static Double d = new Double(0d);
	public final static BigDecimal bd = new BigDecimal(0);

	ValueKit() {
	}

	/**
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public static <T> T getValue(T value, T defaultValue) {
		if (value == null)
			return defaultValue;
		if(value instanceof String)
			return EMPTY.equals(value) ? defaultValue : value;
		if (!(value instanceof Number))
			return value;
		if (value instanceof Byte)
			return value.equals(b) ? defaultValue : value;
		if (value instanceof Short)
			return value.equals(s) ? defaultValue : value;
		if (value instanceof Integer)
			return value.equals(i) ? defaultValue : value;
		if (value instanceof Long)
			return value.equals(l) ? defaultValue : value;
		if (value instanceof Float)
			return value.equals(f) ? defaultValue : value;
		if (value instanceof Double)
			return value.equals(d) ? defaultValue : value;
		if (value instanceof BigDecimal)
			return value.equals(bd) ? defaultValue : value;
		return new BigDecimal(value.toString()).equals(bd) ? defaultValue
				: value;
	}
}
