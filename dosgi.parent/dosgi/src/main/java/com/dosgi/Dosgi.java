package com.dosgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dosgi.kit.ThreadPoolKit;

/**
 * 容器类，放在context工程中，模块里面会用到，不放在agent里面，否则模块还得依赖agent
 * 
 * @author dingnate
 *
 */
public class Dosgi {
	private static transient final Logger LOG = LoggerFactory.getLogger(Dosgi.class);
	private static DosgiLifeCycle lifeCycle = new DosgiLifeCycle();

	/**
	 * 容器包含唯一上下文
	 */
	private static DosgiContextDefault CTX = new DosgiContextDefault();

	public static IDosgiContext context() {
		return CTX;
	}

	/**
	 * @param clazz
	 * @param bean
	 */
	public static void registry(Class<?> clazz, Object bean) {
		CTX.beanFactory().registry(clazz, bean);
	}

	/**
	 * 从上下文取注册服务
	 * 
	 * @param clazz
	 * @return
	 */
	public static <T> T get(Class<T> clazz) {
		return CTX.beanFactory().get(clazz);
	}

	public static void close() {
		CTX.stopModules();
		ThreadPoolKit.stop();
		lifeCycle.setState(LifeCycleState.CLOSED);
		LOG.info("dosgi close success.");
	}

	/**
	 * @return the lifeCycle
	 */
	public static final DosgiLifeCycle getLifeCycle() {
		return lifeCycle;
	}
}
